

export const config = {
    template : "./template.html",
    clickToCopySelector : ".cla-click-to-copy",
    modalTrigger : ".cla-open-modal",
    modalClass   : ".cla-modal",
    finishClass  : ".cla-finish-button"
}

export default config;
