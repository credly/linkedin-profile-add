
import mustache from 'Mustache';

class TemplateLoader{

    render(template, params){

        params.certification.shortUrl = params.certification.url.substring(0, 35) + '...';

        params.certification.dateText = '';

        if(params.certification.date.to.toLowerCase() === 'present')
        {
            params.certification.dateText = '(Check the box to indicate that this badge does not expire)';
        }

        return mustache.render(template, params); 
    }
}

export default TemplateLoader;