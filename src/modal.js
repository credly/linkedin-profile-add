import clipboard from 'Clipboard';

let w = false;

class Modal{
    constructor($container, config){

        this.$container = $container.hide();

        this.config = config;

        this.isOpen = false;

        this.data = {};

        this.copyCallback = false;
    }
    setData(data){

        this.data = data;

        return this;
    }
    setCopyCallback(callback){

        this.copyCallback = callback;

        return this;
    }
    setContent(content){

        this.$container.html( content );

        this.bindContentListeners(this.$container);

        return this;
    }
    bindContentListeners($container){

        let $copyButton = $container.find( this.config.clickToCopySelector );

        let $notif = false;

        let callback = this.copyCallback;

        let data = this.data;

        $copyButton
            .each(function(num, el){

                new clipboard( el );
            })
            .click(function(){

                let text = $(this).find('.cla-block__heading').text().trim();

                if($notif) $notif.removeClass('open');

                $notif = $(".cla-copy-notif")
                    .html('Copied <strong>'+text+'</strong> to Clipboard')
                    .addClass('open');

                if( callback ) callback( text, data, $container );

                setTimeout(function(){

                    $notif.removeClass('open');
                }, 2500);
            });
    }
    open(){

        this.$container.show();

        let $modal = this.$container.find( this.config.modalClass );

        this.isOpen = true;

        let _this = this;

        $modal
            .animate({
                opacity: 1
            }, 300 );

        if(!w)
        {
            setTimeout(function(){

                if(_this.isOpen && !w)
                {
                    w = window.open(
                        'https://www.linkedin.com/profile/add?startTask=CERTIFICATION_NAME', 
                        '_blank',
                        'height=740,width=770,top=50,left=50');
                }

            }, 3000 );
        }

        setTimeout(function(){

            $('html,body').css('overflow', 'hidden');

        }, 1000);

    }
    close(){

        this.$container.find( this.config.modalClass )
            .animate({
                opacity: 0
            }, 300 );

        this.isOpen = false;     

        this.$container.hide();

        setTimeout(function(){

            $('html,body').css('overflow', 'auto');

            if(w)
            {
              w.close();
              w = false;  
            }             

        }, 1000 );        
    }
}

export default Modal;