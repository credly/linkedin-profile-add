//include jquery

import $ from 'jquery';
import jQuery from 'jquery';
window.$ = $;
window.jQuery = jQuery;

//import lib

import config from '../cly-linkedin-add.js';
import loader from './template-loader.js';
import modal from './modal.js';
import html from './template.html';
require('./styles/style.scss');


(function( $ ){

    let plugin = false;
    let copyCallback = false;

    var methods = {
        init : function(options) {

            let template     = new loader();
            let ui           = new modal($(this), config);
            let content      = null;
            let $container   = $(this); 
            plugin       = this;

            plugin.opts      = $.extend({
                modalTrigger : config.modalTrigger,
                closeModalTrigger : config.closeModalTrigger
            }, options ); 

            plugin.getUi = function(){

                return ui;
            };

            /**
             * { function_description }
             *
             * @param      {<object>}  data    The data for parsing template
             */

            plugin.populateModal = function(data){

                ui.setData(data);

                content = template.render( html, data );

                ui.setContent( content );
            };  

            /**
             * { function_description }
             */

            plugin.activateCloseTrigger = function(){

                $container.find(plugin.opts.closeModalTrigger).click(ui.close.bind(ui));
            };

            /**
             * { item_description }
             */

            $(plugin.opts.modalTrigger).click(function(e){

                e.preventDefault();

                let data = $(this).data('profile');

                plugin.open(data);
            });

            /**
             * { item_description }
             */

            $(plugin.opts.closeModalTrigger).click(function(){

                ui.close();
            });

            plugin.open = function(data){

                ui.setCopyCallback(copyCallback);

                plugin.populateModal(data);

                plugin.activateCloseTrigger();

                ui.open();
            };
        },
        open : function( data ) {  

            plugin.open(data);
        },
        onCopy : function( callback ){

            copyCallback = callback;
        } 
    };

    $.fn.credlyLinkedInAddToProfile = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.credlyLinkedInAddToProfile' );
        }    
    };


})( jQuery );


